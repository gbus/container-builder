Docker image builder
=

Problem
-
If you need to create several images based on the same Dockerfile, but based on different distros, distro versions or parameters you need to create and maintain one Dockerfile for each variation. To change any part of your build workflow you would need to change one by one all the Dockerfiles.

Solution
-
With a combination of tools it is possible to generate a single Dockerfile and build multiple images with a couple of shell commands:

    $ jinja2 Dockerfile.tmpl data.yaml -o Dockerfile
    $ ./build_images.sh

That is:
  1. Generate a Dockerfile with multiple image definitions
  2. Create images for each image defined in data.yaml.

Configure the environment
-
    python3 -m venv venv
    source ./venv/bin/activate

    pip install jq yq jinja2-cli

Write your own Dockerfile template
-
Starting from the example Dockerfile.tmpl, add any additional steps as required ensuring that any difference between images is parametrized and resolved by the jinja parser.

Any data specific to an image needs to be added in the data.yaml file.

Advanced usage
-
It is possible to limit the CPU architectures to build for, by listing them in data.yaml:

    platforms:
      - linux/arm64
      - linux/arm/v7
      - linux/amd64

At least one platform must be listed.
