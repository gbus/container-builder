#!/usr/bin/env bash

n_imgs=`yq -r ".images | length" data.yaml`

for ((i=0;i<n_imgs;i++))
do
    img_name=$(yq -r '.images['$i'].name' data.yaml)
    platforms=$(yq -r '.images['$i'].platforms | join(",")' data.yaml)
    docker buildx build --platform $platforms -t $img_name --target $img_name .
done
